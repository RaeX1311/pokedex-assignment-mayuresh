using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject Content;
    [SerializeField] private GameObject cardPrefab;
    [SerializeField] private RawImage visualizeImage;
    [SerializeField] private List<ButtonList> _buttonList;
    [SerializeField] private GameObject HeartButton;
    [SerializeField] private GameObject ScreenShotButton;
    [SerializeField] private GameObject StatsPanel;
    [SerializeField] private GameObject ScrollView;
    [SerializeField] private GameObject FullScreen;
    [SerializeField] private GameObject VisualPanel;

    [Header("--------Stats---------")] [SerializeField]
    private TMP_Text Name;

    [SerializeField] private TMP_Text height;
    [SerializeField] private TMP_Text weight;
    [SerializeField] private TMP_Text abilites;
    [SerializeField] private TMP_Text type;

    #region Singleton

    public static UIManager instance;

    private void Awake()
    {
        Initializer.dataInitialized += InitializeUI;

        if (instance && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
    }

    #endregion
    

    void InitializeUI(Root pokemonData)
    {
        Debug.Log("Initializing UI");
        foreach (var data in pokemonData.pokemonDatas)
        {
           GameObject Go = Instantiate(cardPrefab, Content.transform);
           Go.GetComponent<ButtonID>().SetID(data.id);
           Go.GetComponent<ButtonID>().SetImage(data.spriteURL);
           ButtonList bList = new ButtonList();
           bList.id = data.id;
           bList.button = Go;
           _buttonList.Add(bList);
        }
    }

    public void VisualizeCard(Texture2D texture)
    {
        visualizeImage.texture = texture;
    }

    public void ShowStats(PokemonData pokemonData)
    {
        Name.text ="Name: " + pokemonData.name;
        height.text = "Height: " +  pokemonData.height.ToString();
        weight.text = "Weight: " +  pokemonData.weight.ToString();
        string s = "";
        foreach (var ability in pokemonData.abilities)
        {
            s += ability + ", ";
        }
        abilites.text ="Abilities: "  +  s;
        type.text = "Type: " +  pokemonData.type;
    }

    public void MakeFavorite(int ID)
    {
        Transform currentButton = _buttonList.Where(r => r.id == ID).Select(r => r.button).First().transform;
        currentButton.SetSiblingIndex(0);
    }


    public void ScreenShot()
    {
        HeartButton.SetActive(false);
        ScreenShotButton.SetActive(false);
        ScrollView.SetActive(false);
        StatsPanel.SetActive(false);
        FullScreen.SetActive(true);
        VisualPanel.SetActive(false);
        FullScreen.GetComponent<RawImage>().texture = StateManager.instance.CurrentState.GetCurrentImage();
    }

    public void EnableScreenShot()
    {
        HeartButton.SetActive(true);
        ScreenShotButton.SetActive(true);
        ScrollView.SetActive(true);
        StatsPanel.SetActive(true);
        FullScreen.SetActive(false);
        VisualPanel.SetActive(true);

    }
}

[Serializable]
public class ButtonList
{
    public int id;
    public GameObject button;
}
