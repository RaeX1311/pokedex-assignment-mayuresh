using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonID : MonoBehaviour
{
    [SerializeField] private int buttonId;

    public void SetID(int id)
    {
        buttonId = id;
    }
    

    public void ChangeState()
    {
        StateManager.instance.SetState(buttonId);
    }
    public void SetImage(string URL)
    {
        GetImage.instance.DownloadImage(URL,SetTexture,OnFailedTexture);
    }

    void SetTexture(Texture2D texture)
    {
        GetComponent<RawImage>().texture = texture;
    }

    void OnFailedTexture(Exception e)
    {
        Debug.LogError("Cant Fetch Image. Error: " + e.Message);
    }
}
