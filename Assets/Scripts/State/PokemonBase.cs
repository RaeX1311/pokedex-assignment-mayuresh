using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class PokemonBase : MonoBehaviour
{
    [SerializeField] private PokemonData _pokemonData;
    [SerializeField] private bool favorite;
    [SerializeField] private Texture2D pokemonTexture;
    
    public virtual void Enter()
    {
        UIManager.instance.VisualizeCard(pokemonTexture);
        UIManager.instance.ShowStats(_pokemonData);
    }

    public virtual void Exit()
    {
        
    }

    public void SetData(PokemonData pokemonData)
    {
        _pokemonData = pokemonData;
    }

    public void SetImage(string URL)
    {
        GetImage.instance.DownloadImage(URL,SetTexture,OnFailedTexture);
    }

    void SetTexture(Texture2D texture)
    {
        pokemonTexture = texture;
    }

    void OnFailedTexture(Exception e)
    {
        Debug.LogError("Cant Fetch Image. Error: " + e.Message);
    }

    public void MakeFavorite()
    {
        favorite = true;
    }

    public int GetID()
    {
        return _pokemonData.id;
    }

    public Texture2D GetCurrentImage()
    {
        return pokemonTexture;
    }
}
