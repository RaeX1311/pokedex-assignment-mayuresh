using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StateManager : MonoBehaviour
{

    #region Singleton

    public static StateManager instance;

    private void Awake()
    {
        Initializer.dataInitialized += InitializeStates;

        if (instance && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
    }

    #endregion

    private PokemonBase currentState;

    public PokemonBase CurrentState
    {
        get
        {
            return currentState;
        }
        set
        {
            if (currentState != null)
            {
                currentState.Exit();
            }
            currentState = value;
            currentState.Enter();
        }
    }

    [SerializeField] private PokemonBase theCurrentBase;

    [SerializeField] private GameObject statePrefabParent;
    [SerializeField] private GameObject statePrefab;
    [SerializeField] private List<StateList> stateList = new List<StateList>();
    

    void InitializeStates(Root pokemonData)
    {
        Debug.Log("initializing");
        
        foreach (var data in pokemonData.pokemonDatas)
        {
            GameObject Go = Instantiate(statePrefab, statePrefabParent.transform);
            StateList sl = new StateList();
            sl.ID = data.id;
            PokemonBase concrete = Go.GetComponent<PokemonConcrete>();
            sl.state = concrete;
            concrete.SetData(data);
            concrete.SetImage(data.spriteURL);
            stateList.Add(sl);
        }
    }

    public void SetState(int id)
    {
        
        CurrentState = stateList.Where(r => r.ID == id).Select(r=>r.state).First();
        theCurrentBase = CurrentState;
    }

    public void SetFavorite()
    {
        currentState.MakeFavorite();
        UIManager.instance.MakeFavorite(currentState.GetID());
    }
}

[Serializable]
public class StateList
{
    public int ID;
    public PokemonBase state;
}
