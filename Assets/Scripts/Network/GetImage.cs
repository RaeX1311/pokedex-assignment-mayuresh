using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class GetImage : MonoBehaviour
{

    #region Singleton

    public static GetImage instance;

    private void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
    }

    #endregion

    
    public void DownloadImage(string URL, Action < Texture2D > onSuccess, Action < Exception > onFailure)
    {
        StartCoroutine(IE_ImageDownloader(URL, onSuccess, onFailure));
    }
    
    private IEnumerator IE_ImageDownloader(string imageUrl, Action < Texture2D > onSuccess, Action < Exception > onFailure) {

        UnityWebRequest request = UnityWebRequestTexture.GetTexture(imageUrl);
        yield return request.SendWebRequest();
        
        if (request.result == UnityWebRequest.Result.Success) {
            DownloadHandlerTexture textureDownloadHandler = (DownloadHandlerTexture) request.downloadHandler;
            Texture2D texture = textureDownloadHandler.texture;

            if (texture == null) {
                onFailure?.Invoke(new Exception("image not available"));
                yield break;
            }

            onSuccess?.Invoke(texture);
            yield break;
        }
        
        onFailure?.Invoke(new Exception(request.error));
    }
}
