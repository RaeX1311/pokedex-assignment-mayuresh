using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initializer : MonoBehaviour
{
    [SerializeField] private TextAsset pokemonJsonData;
    [SerializeField] private Root pokemonData;

    public delegate void DataInitialized(Root pokemonData);

    public static DataInitialized dataInitialized;
    private void Start()
    {
        string jsonData = pokemonJsonData.text;
        pokemonData = JsonUtility.FromJson<Root>(jsonData);
        dataInitialized?.Invoke(pokemonData);
    }
}

[Serializable]
public class PokemonData
{
    public int id ;
    public string name ;
    public int height ;
    public int weight ;
    public string spriteURL ;
    public List<string> abilities ;
    public string type ;
}

[Serializable]
public class Root
{
    public List<PokemonData> pokemonDatas ;
}

